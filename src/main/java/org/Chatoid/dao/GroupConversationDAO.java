package org.Chatoid.dao;

import java.util.List;

import org.Chatoid.model.GroupConversation;
import org.Chatoid.utils.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

public class GroupConversationDAO {
	public void addGroupConversation(GroupConversation conversation){
	    Session session = HibernateUtil.getSessionFactory().openSession();
	    session.beginTransaction();
	    session.save(conversation);
	    session.getTransaction().commit();
	    session.close();	
	}
	
	public List<GroupConversation> getGroupConversation(GroupConversation conversation){
	    Session session = HibernateUtil.getSessionFactory().openSession();
	    session.beginTransaction();
	    String sql = " FROM GroupConversation where grupID = :groupID ";
        Query query = session.createQuery(sql);
        query.setParameter("groupID", conversation.getGrupID());
                
        @SuppressWarnings("unchecked")
		List<GroupConversation> list = query.list();   
        System.out.println("Groups: " + list.toString());        
        session.close();    
        return list;	
	}
}
