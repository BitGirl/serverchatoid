package org.Chatoid.controller;

import java.util.List;

import org.Chatoid.dao.*;
import org.Chatoid.model.GroupMembers;
import org.Chatoid.model.Notification;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import org.Chatoid.model.Result;


@RestController
public class GroupMembersController {	
	
	GroupMembersDAO groupMembersDAO = new GroupMembersDAO();  
	NotificationDAO notificationDAO = new NotificationDAO();
    
    @RequestMapping(value="/getGroups", method=RequestMethod.POST)
    @ResponseBody
    public Result getGroups(@RequestBody Result request) {
        System.out.println(request.getResult());
        //currentConversation = request;
        List<GroupMembers> list = groupMembersDAO.getGroups(request.getResult());
        String result = "";
        for(int i = 0; i < list.size(); i++) {
       		result += list.get(i).getGroupID() + " ";
        }
        System.out.println("Result" + result);
        Result res;
        if(result != "")
           res = new Result(result);
        else
        	res = null;
    	return res;
    }
    
    @RequestMapping(value="/addMember", method=RequestMethod.POST)
    @ResponseBody
    public GroupMembers addMember(@RequestBody GroupMembers request) {
        System.out.println(request.toString());
        //currentConversation = request;
        groupMembersDAO.addMember(request);
        notificationDAO.addNotification(new Notification(request.getMemberID(), request.getGroupID()));
    	return request;
    }
    
    @RequestMapping(value="/getMembers", method=RequestMethod.POST)
    @ResponseBody
    public Result getMembers(@RequestBody Result request) {
        System.out.println(request.getResult());
        List<GroupMembers> list = groupMembersDAO.getMembers(request.getResult());
        String result = "";
        for(int i = 0; i < list.size(); i++) {
       		result += list.get(i).getMemberID() + " ";
        }
        System.out.println("Result" + result);
        Result res;
        if(result != "")
           res = new Result(result);
        else
        	res = null;
    	return res;
    }
    
    @RequestMapping(value="/leaveGroup", method=RequestMethod.POST)
    @ResponseBody
    public GroupMembers removeMember(@RequestBody GroupMembers request) {        
        groupMembersDAO.removeMember(request);
    	return new GroupMembers("Deleted", "member");
    }
}
