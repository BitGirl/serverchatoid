package org.Chatoid.controller;

import java.util.List;

import org.Chatoid.dao.GroupConversationDAO;
import org.Chatoid.model.GroupConversation;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GroupConversationController {
	
	GroupConversationDAO groupConversationDAO = new GroupConversationDAO();
	
	@RequestMapping(value="/addGroupConversation", method=RequestMethod.POST)
    @ResponseBody
    public GroupConversation addGroupConversation(@RequestBody GroupConversation request) {
        System.out.println(request.toString());
        //currentConversation = request;
        groupConversationDAO.addGroupConversation(request);
    	return null;
    }
	
	@RequestMapping(value="/getGroupConversation", method=RequestMethod.POST)
    @ResponseBody
    public GroupConversation getGroupConversation(@RequestBody GroupConversation request) {
        System.out.println(request.toString());
        //currentConversation = request;
        List<GroupConversation> list =  groupConversationDAO.getGroupConversation(request);
        String result = "";
        if(list.size() > 0)
	        for (int i = 0; i < list.size(); i++)
	        	result += list.get(i).getContent() + ";";
        else
        	return null;
    	return new GroupConversation(request.getGrupID(), result);
    }

}
