package org.Chatoid.controller;

import java.util.List;

import org.Chatoid.dao.NotificationDAO;
import org.Chatoid.model.Conversation;
import org.Chatoid.model.GroupMembers;
import org.Chatoid.model.Notification;
import org.Chatoid.model.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NotificationController {
	
	NotificationDAO notificationDAO = new NotificationDAO();
	
    @RequestMapping(value="/getNotifications", method=RequestMethod.POST)
    @ResponseBody
    public Notification getNotifications(@RequestBody Notification request) {
        System.out.println(request.toString());        
        List<Notification> list = notificationDAO.getNotifications(request.getUser());
    	    	
        String result = "";
        for(int i = 0; i < list.size(); i++) {
       		result += list.get(i).getContent() + ";";
        }
        System.out.println("Result" + result);
        Notification res;
        if(result != "")
           res = new Notification(request.getUser(), result );
        else
        	res = null;
    	return res;
    }
}
