package org.Chatoid.controller;

import org.Chatoid.model.Conversation;

import java.util.List;

import org.Chatoid.dao.*;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConversationController {

    private ConversationDAO conversationDAO = new ConversationDAO();
    
    /**
     * Takes messages from Database and sends it to the Android Client
     * @param request
     * @return Conversation 
     */
    @RequestMapping(value="/getAllConversations", method=RequestMethod.POST)
    @ResponseBody
    public Conversation getAllConversations(@RequestBody Conversation request) {
        System.out.println(request.toString());
        List<Conversation> list = conversationDAO.getConversations(request.getExpeditor(), request.getDestinatar());
    	String message = "";    	
    	for(int i = 0; i < list.size(); i++){
    		message += list.get(i).getExpeditor()+ ";" + list.get(i).getContent() + ";";
    	}
        return new Conversation(request.getExpeditor(),
        		request.getDestinatar(), message);
    }
    
    
    /**
     * Receives a conversation from Android Client
     * and inserts the conversation in Database
     * @param request
     * @return Conversation
     */
    @RequestMapping(value="/conversation", method=RequestMethod.POST)
    @ResponseBody
    public Conversation setConversation(@RequestBody Conversation request) {
        System.out.println(request.toString());        
	    conversationDAO.addConversation(request);
    	return null;
    }    
}
