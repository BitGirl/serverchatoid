package org.Chatoid.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.Chatoid.dao.NotificationDAO;
import org.Chatoid.model.Notification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class FileUploadController {
	
	@RequestMapping(value = "/uploadPhoto/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> uploadPhoto(@RequestParam("file") MultipartFile srcFile,
                                               @PathVariable("id") String id) throws IllegalStateException, Exception {

		System.out.println("File Upload");
		srcFile.transferTo(new File("C:/Users/adriana/Downloads/gs-rest-service-master/gs-rest-service-master/complete/recvFiles/recv.pdf"));
        return null;
    }
}
