package org.Chatoid.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="notification")
public class Notification {
	@Id
	@Column(name="id")
	private int id;
	@Column(name="user")
	private String user;
	@Column(name="content")
	private String content;
	
	public Notification() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Notification(String user, String content) {
		super();
		this.user = user;
		this.content = content;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public String getUser() {
		return user;
	}
	
	public void setUser(String user) {
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Notification [user=" + user + ", content=" + content + "]";
	}
	
	
}
