package org.Chatoid.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="conversation")
public class Conversation {
	@Id
	@Column(name="id")
	private int id; 
	@Column(name="expeditor")
	private String expeditor;
	@Column(name="destinatar")
    private String destinatar;
	@Column(name="content")
    private String content;

    public Conversation() {
    }

    public Conversation(String expeditor, String destinatar, String content) {
        this.expeditor = expeditor;
        this.destinatar = destinatar;
        this.content = content;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getExpeditor() {
        return expeditor;
    }

    public void setExpeditor(String expeditor) {
        this.expeditor = expeditor;
    }

    public String getDestinatar() {
        return destinatar;
    }

    public void setDestinatar(String destinatar) {
        this.destinatar = destinatar;
    }

	@Override
	public String toString() {
		return "Conversation [expeditor=" + expeditor + ", destinatar=" + destinatar + ", content=" + content + "]";
	}
}
